# Med application

Deploy [here](https://daniil_khlyvniuk.gitlab.io/med-application)

Single-page application on which the secretary can create cards of appointments for doctors.

#### Functional:

1. Login
2. Create cards
3. Remove cards
4. Edit cards
5. Drag&Drop (the new order will not be saved on reboot)
6. Filter cards by doctors, description and priority

## Authors:

#### Aleksandr Mayboroda

- all classes Visit-
- api methods
- svg

#### Oleksii Lubianyi

- classes: Card, CardCardio, CardDentist, CardTherapist
- delete cards
- showMore in cards

#### Daniil Khlyvniuk

- login/logout
- render user's cards after login
- classes: Modal, Messages
- filter cards
- webpack-config
- Drag&Drop

---

### Technologies we used:

1. html5
2. css3 / scss
3. JS ES6
4. Bootstrap 4
5. Webpack 5
6. Git
7. [GitLab pages](https://daniil_khlyvniuk.gitlab.io/med-application)

---

### Production Build

```bash
npm run build
```

### Development Build

```bash
npm run build-dev
```

### Development Build, watching for file changes

```bash
npm run watch
```

### Development Server

```bash
npm start
```
