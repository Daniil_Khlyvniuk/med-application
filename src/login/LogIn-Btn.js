import {LoginForm} from "./LoginForm";
import Modal from "../script/modules/Modal"
import Button from "../script/modules/Button"


class LogInButton extends Button{
    constructor(buttonID, text) {
        super(buttonID, text);
    }
    handleClick(){
        const form = new LoginForm().createForm()
        new Modal(form).render()
    }

    render() {
        this.button.addEventListener("click", e=>{
            e.preventDefault()
            this.handleClick()
        })
    super.render()
    }
}

export { LogInButton }


