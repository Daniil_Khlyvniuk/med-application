import API from "../script/modules/api";
import CardList from "../CardList/Cardlist.js";
import Message from "../script/modules/MessegeGenerator"

export default class Card {
  parent = document.querySelector(".card-list");

  constructor({ userName, title, id }, location) {
    this.userName = userName;
    this.title = title;
    this.id = id;
    this.location = location;
  }
  _ELEMENTS = {
    btnLoad: document.createElement("button"),
    btnUnload: document.createElement("button"),
    itemsList: document.createElement("ul"),
    cardWrapper: document.createElement("div"),
    btnWrapp: document.createElement("div"),
    btnEdit: document.createElement("button"),
    btnClose: document.createElement("button"),
  };

  render() {
    const {
      itemsList,
      btnWrapp,
      cardWrapper,
      btnLoad,
      btnEdit,
      btnClose,
      btnUnload,
    } = this._ELEMENTS;

    Message.delete()

    itemsList.insertAdjacentHTML(
      "afterbegin",
      ` <li class="card-username">Ф.И.О. : <span>${this.userName}</span></li>
<li class="card-title">Название визита: ${this.title}</li>`
    );
    btnUnload.style.display = "none";
    btnUnload.innerText = "Unload";
    btnUnload.className = "card-button card-load";
    btnLoad.innerText = "Load more ";
    btnLoad.className = "card-button card-load";
    cardWrapper.className = "card drag-item";
    cardWrapper.setAttribute("draggable", "true");
    itemsList.className = "card-list";
    btnWrapp.className = "card-buttons";
    btnEdit.innerText = "Edit card";
    btnEdit.className = "card-button card-edit";
    btnClose.className = "btn-close";

    this.parent.insertAdjacentElement(this.location, cardWrapper);
    cardWrapper.append(btnClose);
    cardWrapper.append(itemsList);
    cardWrapper.append(btnWrapp);
    btnWrapp.append(btnLoad);
    btnWrapp.append(btnEdit);
    btnWrapp.prepend(btnUnload);

    btnLoad.addEventListener("click", (e) => {
      e.preventDefault();
      this.showMore();
      this.changeBtn();
      
    });
    btnUnload.addEventListener("click", (e) => {
      e.preventDefault();
      this.showLess();
      this.changeBtnBack();
    });
    btnEdit.addEventListener("click", (e) => {
      e.preventDefault();
      this.cardEdit();
    });
    btnClose.addEventListener("click", async (e) => {
      e.preventDefault();
      try {
        if (!confirm("Ви дійсно бажаєте видалити цю картку")) return;
        await API.deleteCard(this.id);
        cardWrapper.remove();
        await CardList.setUserCards();
      } catch (error) {
        alert(error);
      }
    });
  }
  showMore() {}
  changeBtn() {
    const { btnLoad, btnUnload } = this._ELEMENTS;
    btnLoad.style.display = "none";
    btnUnload.style.display = "inline-block";
  }
  showLess() {
    const { itemsList } = this._ELEMENTS;

    const children = [...itemsList.children];
    children.forEach((element) => {
      if (element.classList.contains("card-item")) {
        element.remove();
      }
    });
  }
  changeBtnBack() {
    const { btnLoad, btnUnload } = this._ELEMENTS;
    btnLoad.style.display = "inline-block";
    btnUnload.style.display = "none";
  }
  choosePriority() {
    if (this.priority === "normal") {
      return (this.priority = "Нормальная");
    } else if (this.priority === "prioritet") {
      return (this.priority = "Приоритетная");
    } else if (this.priority === "main") {
      return (this.priority = "Неотложная");
    }
  }
  cardEdit() {}
}
