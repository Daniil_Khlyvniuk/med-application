import Message from "./MessegeGenerator"

export default class Form {
  _ELEMENTS = {
    _form: document.createElement("form"),
  };

  _BUTTONS = {
    ___submit: document.createElement("button"),
    ___close: document.createElement("button"),
  };

  _ASSETS = {
    _form: {
      className: "",
      id: "form_id",
      handler: {
        name: "submit",
        fun: (ev) => {
          ev.preventDefault();
          //получаем данные форм и зкаидываем в обюъект класса
          let formData = new FormData(ev.target);
          this.formFields(formData); //закидываем в объект ключ-значение

          //ПЕРЕДАТЬ сюда функцию-обоработчик из корня класса
          this.formSubmit(ev.target);
        },
      },
    },
    ___submit: {
      type: "submit",
      className: "btn btn-success me-1",
      innerText: "Сохранить",
    },
    ___close: {
      type: "button", //Он ставит по умолч. SUBMIT
      className: "btn btn-danger me-1",
      innerText: "Закрыть",
      handler: {
        name: "click",
        fun: (ev) => {
          // console.log("Закрыть модалку!", ev.target.type);
          //ПЕРЕДАТЬ сюда функцию-обоработчик из корня класса
        },
      },
    },
  };

  //for validate rules
  _VALIDATE_RULES = {
    // fieldKey: [{ funct: () => {}, error: "" },{ funct: () => {}, error: "" }],
  };

  //for validate errors: {fieldName: error}
  _VALIDATE_ERRORS = {};

  _FORM_INPUTS = {};

  //if is valid (without errors)
  _IS_VALID = true;

  addButtonsToForm() {
    for (let key in this._BUTTONS) {
      this._BUTTONS[key];
      this.elementRender(this._BUTTONS[key], key);
      this._ELEMENTS._form.append(this._BUTTONS[key]);
    }
  }

  /**
   *
   * @returns form dom element
   */
  createForm() {
    if (Object.keys(this._ELEMENTS).length > 0) {
      for (let key in this._ELEMENTS) {
        if (key.indexOf("__") === 0) continue;
        this.elementRender(this._ELEMENTS[key], key);
      }

      for (let key in this._ELEMENTS) {
        if (key.indexOf("__") === 0 || key.indexOf("_") === 0) continue;
        this._ELEMENTS._form.append(this._ELEMENTS[key]);
      }

      this.addButtonsToForm();

      return this._ELEMENTS._form;
    }
  }

  elementRender(element, key = "") {
    let elementAssets = this._ASSETS[key];
    if (elementAssets) {
      let {
        id,
        className,
        placeHolder,
        name,
        options,
        type,
        handler,
        attributes,
        innerText,
        _defaultValue,
      } = elementAssets;

      if (name) {
        element.name = name;
      }
      if (id) {
        element.id = id;
      }
      if (className) {
        element.className = className;
      }
      if (placeHolder) {
        element.placeHolder = placeHolder;
      }
      if (type) {
        element.type = type;
      }
      if (options) {
        this.optionDomRender(element, options, _defaultValue);
      }
      if (attributes) {
        attributes.forEach((attr) => {
          element.setAttribute(attr.name, attr.value);
        });
      }
      if (handler) {
        element.addEventListener(handler.name, handler.fun);
      }
      if (innerText) {
        element.innerText = innerText;
      }

      //insert value Input & Textarea
      if (_defaultValue) {
        switch (element.tagName) {
          case "INPUT":
            element.setAttribute("value", _defaultValue);
            break;
          case "TEXTAREA":
            element.innerText = _defaultValue;
            break;
        }
      }
    }
  }

  optionDomRender(select, options, _defaultValue = "") {
    let { className, elements, emptyValue } = options;
    if (emptyValue) {
      select.insertAdjacentHTML(
        "afterbegin",
        _defaultValue
          ? `<option class="${className}" disabled>${emptyValue}</option>`
          : `<option class="${className}" selected disabled>${emptyValue}</option>`
      );
    }
    select.insertAdjacentHTML(
      "beforeend",
      elements
        .map((opt) => {
          let optn =
            _defaultValue && _defaultValue === opt.value
              ? `<option class="${className}" value="${opt.value}" selected>${opt.text}</option>`
              : `<option class="${className}" value="${opt.value}">${opt.text}</option>`;
          return optn;
        })
        .join("")
    );
  }

  getCurrentDate(format = "") {
    let d = new Date(),
      result,
      date = d.getDate() < 10 ? "0" + d.getDate() : d.getDate(),
      month =
        d.getMonth() + 1 < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1,
      year = d.getFullYear();
    switch (format) {
      case "Y-m-d":
        result = `${year}-${month}-${date}`;
        break;
      default:
        result = d;
    }
    return result;
  }

  formSubmit(form) {
    //here submit handler code
  }

  formValidate() {
    //here validate handler code
  }

  makeFirstLetterToUpper(string) {
    let name = string[0].toUpperCase();
    return (name += string.slice(1));
  }

  /**
   * @makes form Inputs to object
   */
  formFields(form) {
    for (let key of form.entries()) {
      this._FORM_INPUTS[key[0]] = key[1];
    }
  }

  /**
   * @insert to _ASSETS[key] default value
   */
  //устанавливаем значения, чтобы получить форму с заполненными полями
  setValues(valuesObject) {
    if (Object.keys(valuesObject).length > 0) {
      for (let key in valuesObject) {
        if (this._ASSETS[key]) {
          this._ASSETS[key]._defaultValue = valuesObject[key];
        }
      }
    }
  }

  /**
   *@deletes form with standart id "visit_form"
   */
  formDelete() {
    let fromElem = document.getElementById(this._ASSETS._form.id);
    if (fromElem) {
      fromElem.remove();
    }
  }
}
