export default class Message {
    static showMessage(parentElem, location, text){
        // if (parentElem.querySelector(".error-mess")) return
        parentElem.insertAdjacentHTML(location, `
        <p class="__message">${text}</p>
        `)
    }
    static showError(parentElem, location, text){
        // if (parentElem.querySelector(".error-mess")) return
        parentElem.insertAdjacentHTML(location, `
        <p class="error-mess __message">${text}</p>
        `)
    }
    static delete(parentElem){
        parentElem = parentElem || document
        parentElem.querySelectorAll(".__message").forEach(el=> el.remove())
    }
}