export default class Button {
    parent =  document.querySelector(".buttonWrapper")
    button = document.createElement("button")
    constructor(buttonID, text) {
        this.buttonID = buttonID
        this.text = text
    }

    render(){
        this.button.className = "px-4 btn btn-outline-success my-2 my-sm-0"
        this.button.setAttribute("id", this.buttonID)
        this.button.innerText = this.text
        this.parent.append( this.button)
    }
}