export default class Modal {
  _ELEMENTS = {
    modalBG: document.createElement("div"),
    modalWindow: document.createElement("div"),
    closeBtn: document.createElement("button"),
  };

  constructor(childrenElem) {
    this.children = childrenElem;
  }

  render() {
    const { modalBG, modalWindow, closeBtn } = this._ELEMENTS;

    modalBG.setAttribute("id", "modalBG");
    modalBG.classList.add("modal-bg");
    modalWindow.className =
      "modalWindow col-xxl-2  col-xl-4 col-lg-4 col-md-5 col-sm-10 col-10 bg-white rounded";
    modalWindow.style.padding = "2em";
    closeBtn.style.clipPath =
      "polygon(20% 0%, 0% 20%, 30% 50%, 0% 80%, 20% 100%, 50% 70%, 80% 100%, 100% 80%, 70% 50%, 100% 20%, 80% 0%, 50% 30%)";
    closeBtn.style.backgroundColor = "crimson";
    closeBtn.style.position = "absolute";
    closeBtn.style.top = ".5em";
    closeBtn.style.right = ".5em";
    closeBtn.style.width = "1.3em";
    closeBtn.style.height = "1.3em";
    closeBtn.style.border = "none";

    modalWindow.append(this.children);
    modalWindow.append(closeBtn);
    modalBG.append(modalWindow);

    modalBG.addEventListener("click", (e) => {
      this.handleClose(e);
    });

    closeBtn.addEventListener("click", (e) => {
      e.preventDefault();
      this.destroy();
    });

    document.body.prepend(modalBG);
  }

  static destroy() {
    document.querySelector("#modalBG").remove();
  }

  destroy() {
    document.querySelector("#modalBG").remove();
  }

  handleClose(e) {
    if (
      e.target === e.currentTarget ||
      e.target === document.querySelector(".closeFormBtn")
    )
      return this.destroy();
  }
}
