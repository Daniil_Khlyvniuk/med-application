import VisitCardiologist from "./VisitCardiologist";
import API from "../../api.js";
import Modal from "../../Modal.js";
import Message from "../../MessegeGenerator.js";
import {CardCardio} from '../../../../card/cardClasses.js'
import CardList from "../../../../CardList/Cardlist";

export default class VisitCardiologistCreate extends VisitCardiologist
{
    constructor(doctor) {
        super();
        this.additionaValues = {
          doctor,
          title: "Визит к Кардиологу",
        };
    }

    async formSubmit(formData) {
        let form = this._ELEMENTS._form;
        Message.delete(form);
        if (!this.formValidate(formData)) {
          for (let err in this._VALIDATE_ERRORS) {
            Message.showError(form, "beforeend", this._VALIDATE_ERRORS[err]);
          }
        } else {
          try {
            let { doctor, title } = this.additionaValues;
            let response = await API.addCard({
              // title: "Визит к Кардиологу",
              title,
              doctor,
              ...this._FORM_INPUTS,
            }).then((resp) => resp.json());
            new CardCardio(response, "afterbegin").render();
              CardList.dragAndDrop()
          } catch (er) {
            alert("Sorry, try later");
          }
    
          //скрываем popup
          Modal.destroy();
        }
      }
}