import VisitTherapist from "./VisitTherapist.js";
import API from "../../api.js";
import Modal from "../../Modal.js";
import Message from "../../MessegeGenerator.js";
import {CardTerapist} from '../../../../card/cardClasses.js'
import CardList from "../../../../CardList/Cardlist";

export default class VisitTherapistCreate extends VisitTherapist {
    constructor(doctor) {
      super();
      this.additionaValues = {
        doctor,
        title: "Визит к Терапевту",
      };
    }

    async formSubmit(formData) {
      let form = this._ELEMENTS._form;

      Message.delete(form);
      if (!this.formValidate(formData)) {
        for (let err in this._VALIDATE_ERRORS) {
          Message.showError(form, "beforeend", this._VALIDATE_ERRORS[err]);
        }
      } else {
        try {
          let { doctor, title } = this.additionaValues;
          let response = await API.addCard({
            // title: "Визит к Терапевту",
            title,
            doctor,
            ...this._FORM_INPUTS,
          }).then((resp) => resp.json());
          new CardTerapist(response, "afterbegin").render();
          CardList.dragAndDrop()
        } catch (er) {
          alert("Sorry, try later");
        }
  
        //скрываем popup
        Modal.destroy();
      }
    }

  }