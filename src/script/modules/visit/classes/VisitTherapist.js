import Visit from "./Visit.js";
import API from "../../api.js";
import Modal from "../../Modal.js";
import Message from "../../MessegeGenerator.js";

export default class VisitTherapist extends Visit {
  constructor() {
    super();

    this._ELEMENTS = {
      ...this._ELEMENTS,
      ageLabel: document.createElement("label"),
      age: document.createElement("input"), //возраст
    };
    this._ASSETS = {
      ...this._ASSETS,
      age: {
        className: "form-control mb-3",
        name: "age",
        id: "age",
        type: "text",
        attributes: [{ name: "placeHolder", value: "Возраст" }],
      },
      ageLabel: {
        className: "form-label mb-1",
        innerText: "Укажите возраст (полных лет)",
        attributes: [{ name: "for", value: "age" }],
      },
    };
  }

  formValidate(formData) {
    super.formValidate(formData);
    let { age } = this._FORM_INPUTS;

    if (isNaN(age) || +age <= 0 || +age > 100 || +age % 1 !== 0) {
      this._VALIDATE_ERRORS["age"] =
        "Не корректный возраст - должно быть целое число от 1 до 100";
    }

    return (this._IS_VALID =
      Object.keys(this._VALIDATE_ERRORS).length > 0 ? false : true);
  }
}
