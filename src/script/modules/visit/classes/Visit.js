import Form from "../../Form.js";
import Modal from "../../Modal.js";

export default class Visit extends Form {
  constructor() {
    super();

    this._ELEMENTS = {
      ...this._ELEMENTS,
      goalLabel: document.createElement("label"),
      goal: document.createElement("textarea"),
      descriptionLabel: document.createElement("label"),
      description: document.createElement("textarea"),
      priorityLabel: document.createElement("label"),
      priority: document.createElement("select"),
      userNameLabel: document.createElement("label"),
      userName: document.createElement("input"),
    };

    this._ASSETS = {
      ...this._ASSETS,
      _form: {
        //передаю сюда, потому как id может быть разным
        className: "visit-form",
        id: "visit_form",
        handler: {
          name: "submit",
          fun: (ev) => {
            ev.preventDefault();

            //получаем данные форм и зкаидываем в обюъект класса, т.к. перезаписали родительскиую форму
            let formData = new FormData(ev.target);
            this.formFields(formData) //закидываем в объект ключ-значение

            //ПЕРЕДАТЬ сюда функцию-обоработчик из корня класса
            this.formSubmit(formData);
          },
        },
      },
      ___close: {
        type: "button", //Он ставит по умолч. SUBMIT
        className: "btn btn-danger me-1",
        innerText: "Закрыть",
        handler: {
          name: "click",
          fun: (ev) => {
            //ПЕРЕДАТЬ сюда функцию-обоработчик из корня класса
            this.popupClose()
          },
        },
      },
      goalLabel: {
        className: "form-label mb-1",
        innerText: "Цель визита:",
        attributes: [{ name: "for", value: "goal" }],
      },
      goal: {
        name: "goal",
        id: "goal",
        className: "form-control mb-3",
        attributes: [{ name: "placeHolder", value: "Опишите цель визита" }],
      },
      descriptionLabel: {
        className: "form-label mb-1",
        innerText: "Описание:",
        attributes: [{ name: "for", value: "description" }],
      },
      description: {
        name: "description",
        id: "description",
        className: "form-control mb-3",
        placeHolder: "Описание",
        attributes: [{ name: "placeHolder", value: "Укажите Описание" }],
      },
      priorityLabel: {
        className: "form-label mb-1",
        innerText: "Приоритет:",
        attributes: [{ name: "for", value: "priority" }],
      },
      priority: {
        name: "priority",
        id: "priority",
        className: "form-select mb-3",
        options: {
          className: "",
          emptyValue: "Выберите Приоритет", //пустое значение в селект
          elements: [
            { value: "normal", text: "Нормальная" },
            { value: "prioritet", text: "Приоритетная" },
            { value: "main", text: "Неотложная" },
          ],
        },
      },
      userNameLabel: {
        className: "form-label mb-1",
        innerText: "ФИО:",
        attributes: [{ name: "for", value: "userName" }],
      },
      userName: {
        name: "userName",
        id: "userName",
        type: "text",
        className: "form-control mb-3",
        attributes: [{ name: "placeHolder", value: "Укажите ФИО" }],
      },
    };
  }

  formValidate(formData) {
    this._VALIDATE_ERRORS = {};
    let goal = formData.get("goal"),
      description = formData.get("description"),
      priority = formData.get("priority"),
      userName = formData.get("userName");

    if (!goal || goal.trim().length <= 5) {
      this._VALIDATE_ERRORS["goal"] = "Цель должна быть не короче 5 символов";
    }
    // if (!description || description.trim().length <= 10) {
    //   this._VALIDATE_ERRORS["description"] =
    //     "Описание должно быть не короче 10 символов";
    // }
    if (!priority) {
      this._VALIDATE_ERRORS["priority"] = "Не указан приоритет";
    }
    if (!userName || userName.trim().split(" ").length < 2) {
      this._VALIDATE_ERRORS["userName"] =
        "Не указано ФИО клиента или меньше 2х слов";
    }
    return (this._IS_VALID =
      Object.keys(this._VALIDATE_ERRORS).length > 0 ? false : true);
  }

  popupClose()
  {
    Modal.destroy()
  }

}
