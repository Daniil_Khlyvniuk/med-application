import Visit from "./Visit.js";
import API from "../../api.js";
import Modal from "../../Modal.js";
import Message from "../../MessegeGenerator.js";

export default class VisitDentist extends Visit {
  constructor() {
    super();
    this._ELEMENTS = {
      ...this._ELEMENTS,
      lastVisitDateLabel: document.createElement("label"),
      lastVisitDate: document.createElement("input"),
    };
    this._ASSETS = {
      ...this._ASSETS,
      lastVisitDate: {
        className: "form-control mb-3",
        name: "lastVisitDate",
        id: "lastVisitDate",
        type: "date",
        _defaultValue: this.getCurrentDate("Y-m-d"),
      },
      lastVisitDateLabel: {
        className: "form-label mb-1",
        innerText: "Дата поседнего визта:",
        attributes: [{ name: "for", value: "lastVisitDate" }],
      },
    };
  }

  // async formSubmit(formData) {
  //   let form = this._ELEMENTS._form;
  //   Message.delete(form);

  //   if (!this.formValidate(formData)) {
  //     //вывод ошибок
  //     for (let err in this._VALIDATE_ERRORS) {
  //       Message.showError(form, "beforeend", this._VALIDATE_ERRORS[err]);
  //     }
  //   } else {
  //     // 2.1 отправка данных на сервер - переписать под AWAIT
  //     try {
  //       let { doctor, title } = this.additionaValues;
  //       let response = await API.addCard({
  //         title,
  //         doctor,
  //         ...this._FORM_INPUTS,
  //       }).then((resp) => resp.json());
  //       new CardDentist(response, "afterbegin").render();
  //     } catch (er) {
  //       alert("Sorry, try later");
  //     }

  //     //скрываем popup
  //     Modal.destroy();
  //   }
  // }

  formValidate(formData) {
    super.formValidate(formData);

    let { goal, description, priority, userName, lastVisitDate } =
      this._FORM_INPUTS;

    if (!lastVisitDate || lastVisitDate.trim().length <= 0) {
      this._VALIDATE_ERRORS["lastVisitDate"] =
        "Не заполнена дата последнего визита";
    }

    return (this._IS_VALID =
      Object.keys(this._VALIDATE_ERRORS).length > 0 ? false : true);
  }
}
