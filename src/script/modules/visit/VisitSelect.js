import Visit from "./classes/Visit.js";
import VisitDentistCreate from "./classes/VisitDentistCreate";
import VisitCardiologistCreate from "./classes/VisitCardiologistCreate.js";
import VisitTherapistCreate from "./classes/VisitTherapistCreate.js";

export default class VisitSelect extends Visit {
  constructor() {
    super();

    this._ELEMENTS = {
      ...this._ELEMENTS,
      __select: document.createElement("select"),
    };
    this._ASSETS = {
      ...this._ASSETS,
      __select: {
        name: "doctor_select",
        id: "doctor_select",
        className: "form-select mb-3",
        handler: {
          name: "change",
          fun: (ev) => {
            this.selectHandler(ev.target);
          },
        },
        options: {
          className: "",
          emptyValue: "Выберите Доктора", //пустое значение в селект
          elements: [
            { value: "dentist", text: "Дантист" },
            { value: "cardiologist", text: "Кардиолог" },
            { value: "therapist", text: "Терапевт" },
          ],
        },
      },
    };
  }

  /**
   *
   * @returns select dom element
   */
  createSelect() {
    this.elementRender(this._ELEMENTS.__select, "__select");
    return this._ELEMENTS.__select;
  }

  /**
   *
   * @select change handler {doctor_select} select
   * @returns needed form DOM object
   */
  selectHandler(select) {
    let value = select.value.trim(),
      method;
      this.formDelete() //удаляем форму
    if (value) {
      method = `Visit${this.makeFirstLetterToUpper(value)}Action`;
      if (method in this) {
        this._ELEMENTS.__select.insertAdjacentElement('afterend',this[method](value))
      }
    }
  }

  /**
   * @requires import VisitDentistCreate from "./classes/VisitDentistCreate.js";
   */
  VisitDentistAction(doctor) {
    return new VisitDentistCreate(doctor).createForm();
  }

  /**
   * @requires import VisitCardiologistCreate from "./classes/VisitCardiologistCreate.js";
   */
  VisitCardiologistAction(doctor) {
    return new VisitCardiologistCreate(doctor).createForm();
  }

  /**
   * @requires import VisitTherapistCreate from "./classes/VisitTherapistCreate.js";
   */
  VisitTherapistAction(doctor) {
    return new VisitTherapistCreate(doctor).createForm();
  }
}

