import CardList from "../CardList/Cardlist";


export default class FilterForm {
    _ELEMENTS = {
        parent: document.querySelector("#root"),
        formWrapper: document.createElement("div"),
        form: document.createElement("form"),
        selectDoctor: document.createElement("select"),
        selectPriority: document.createElement("select"),
        inputDescription: document.createElement("input"),
        cleanBtn: document.createElement("button"),
        labelDoc: document.createElement("label"),
        labelPrior: document.createElement("label"),
        labelDescr: document.createElement("label"),
    }

    render() {
        const {form, formWrapper, parent,labelDoc, selectDoctor,labelPrior, selectPriority, labelDescr, inputDescription, cleanBtn,} = this._ELEMENTS

        formWrapper.setAttribute("id", "filter")
        form.className ="filter-form d-flex flex-wrap justify-content-between"

        labelDoc.setAttribute("for","choose-doctor")

        selectDoctor.className = "form-select"
        selectDoctor.setAttribute("name", "form-control")
        selectDoctor.setAttribute("id", "choose-doctor")


        labelPrior.setAttribute("for","choose-priority")

        selectPriority.className = "form-select"
        selectPriority.setAttribute("name", "form-control")
        selectPriority.setAttribute("id", "choose-priority")
        inputDescription.className = "form-control"
        inputDescription.setAttribute("id", "description")
        inputDescription.setAttribute("type", "text")

        labelDescr.setAttribute("for","choose-priority")
        cleanBtn.className = "cleanFilterForm btn btn-link my-2 my-sm-0"
        cleanBtn.setAttribute("id", "cleanFilterForm")
        cleanBtn.innerText = "Clean filters"

        selectDoctor.insertAdjacentHTML("afterbegin",
            `
            <option value="all">All</option>
            <option value="cardiologist">Cardiologist</option>
            <option value="dentist">Dentist</option>
            <option value="therapist">Therapist</option>
            `)

        selectPriority.insertAdjacentHTML("afterbegin",
            `
            <option value="all">All</option>
            <option value="normal">Normal</option>
            <option value="main">Middle</option>
            <option value="prioritet">High</option>
            `)

        labelDoc.innerText = "Choose-doctor"
        labelDoc.append(selectDoctor)

        labelPrior.innerText = "Choose-priority"
        labelPrior.append(selectPriority)

        labelDescr.innerText = "Description"
        labelDescr.append(inputDescription)

        selectDoctor.addEventListener("change", ()=>{
            this.handleOnChange()
        })
        selectPriority.addEventListener("change", ()=>{
            this.handleOnChange()
        })

        inputDescription.addEventListener("change", async ev=>{
            ev.preventDefault()
          await this.handleOnChange()
        })

        form.addEventListener("submit", async ev => {
            ev.preventDefault()
            this.clearFilter()
            await CardList.filter({})
        })

        form.append(labelDoc, labelPrior, labelDescr, cleanBtn)
        formWrapper.append(form)

        parent.prepend(formWrapper)
    }

   async handleOnChange(){
        let selectedValue = this.getValue()
       await CardList.filter(selectedValue)
    }

    getValue(){
        const {selectDoctor, selectPriority, inputDescription,} = this._ELEMENTS

        let doc = selectDoctor.value
        let desc = inputDescription.value
        let prop = selectPriority.value

        if (selectDoctor.value === "all") doc = null
        if (selectPriority.value === "all") prop = null
        if (inputDescription.value === "") desc = null

        return {
                doctor: doc,
                description: desc,
                priority: prop,
            }
    }

    static deleteFilterForm(){
        const filterForm = document.querySelector("#filter")
        if (filterForm) return filterForm.remove()
    }

     clearFilter(){
        const {selectDoctor, selectPriority, inputDescription,} = this._ELEMENTS
        selectDoctor.value = ""
        selectPriority.value = ""
        inputDescription.value = ""
    }
}
